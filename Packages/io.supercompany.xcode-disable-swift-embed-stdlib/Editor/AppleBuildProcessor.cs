#if UNITY_IOS || UNITY_TVOS
using System;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace Supercompany.XCodeDisableSwiftEmbedStdlib.Editor
{
    public class AppleBuildProcessor : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder => 1;

        static readonly BuildTarget[] SupportedTargets = {
            BuildTarget.iOS,
            BuildTarget.tvOS,
        };

        public void OnPostprocessBuild(BuildReport report)
        {
            if (!SupportedTargets.Contains(report.summary.platform))
            {
                return;
            }

            var pbxProjPath = PBXProject.GetPBXProjectPath(report.summary.outputPath);
            var pbxProj = new PBXProject();
            pbxProj.ReadFromFile(pbxProjPath);

            pbxProj.SetBuildProperty(pbxProj.GetUnityFrameworkTargetGuid(), "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "NO");

            pbxProj.WriteToFile(pbxProjPath);
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            BuildTarget platform = report.summary.platform;
            switch (platform)
            {
                case BuildTarget.iOS:
                {
                    var minimumVersion = new Version(12, 2);
                    if (Version.TryParse(PlayerSettings.iOS.targetOSVersionString, out Version currentVersion)
                        && currentVersion < minimumVersion
                        && WarnIncompatibleVersion(platform, currentVersion, minimumVersion))
                    {
                        PlayerSettings.iOS.targetOSVersionString = minimumVersion.ToString();
                    }
                    break;
                }

                case BuildTarget.tvOS:
                {
                    var minimumVersion = new Version(12, 2);
                    if (Version.TryParse(PlayerSettings.tvOS.targetOSVersionString, out Version currentVersion)
                        && currentVersion < minimumVersion
                        && WarnIncompatibleVersion(platform, currentVersion, minimumVersion))
                    {
                        PlayerSettings.tvOS.targetOSVersionString = minimumVersion.ToString();
                    }
                    break;
                }
            }
        }

        private bool WarnIncompatibleVersion(BuildTarget platform, Version currentVersion, Version minimumVersion)
        {
            string warningMessage = $"{platform} {currentVersion} is not guaranteed to have Swift runtime libraries installed, your app may crash when running on versions earlier than {minimumVersion}.";
            if (!Application.isBatchMode)
            {
                return EditorUtility.DisplayDialog(
                    "Warning",
                    $"{warningMessage}\nDo you want to upgrade the target SDK to {minimumVersion}?",
                    "Upgrade target SDK",
                    "Cancel"
                );
            }
            else
            {
                Debug.LogWarning(warningMessage);
                return false;
            }
        }
    }
}
#endif