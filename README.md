# Supercompany XCode Disable Swift Embed stdlib
Disables embedding Swift standard libraries in XCode projects and warn about minimum iOS SDK version needed.


## Installing
Install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) using the following URL:
```
https://gitlab.com/super-company-git/supercompany-xcode-disable-swift-embed-stdlib.git?path=/Packages/io.supercompany.xcode-disable-swift-embed-stdlib#1.0.1
```


## How to use
1. Just install the package and you're good to go.
2. Enjoy 🍾
